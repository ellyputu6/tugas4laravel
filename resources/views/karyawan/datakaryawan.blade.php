@extends('layout.main')

@section('title', 'Data Karyawan')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong> DATA KARYAWAN </strong></h3>
                            <div class="right">
                                <a href="{{ url('data_kry/add') }}" class="btn btn-success btn-sm">
                                    <i class="fa fa-plus"></i> Add
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nama</th>
                                        <th>No Karyawan</th>
                                        <th>No Telp</th>
                                        <th>Jabatan</th>
                                        <th>Divisi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->nama_karyawan }}</td>
                                            <td>{{ $item->no_karyawan }}</td>
                                            <td>{{ $item->no_telp_karyawan }}</td>
                                            <td>{{ $item->jabatan_karyawan }}</td>
                                            <td>{{ $item->divisi_karyawan }}</td>
                                            <td>
                                                <a href="{{ url('data_kry/edit/' .$item->id) }}" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <form action="{{ url('data_kry/' .$item->id) }}" method="post" class="d-lg-inline" onclick="return confirm('Yakin Ingin Hapus Data?')">
                                                    @method('delete')
                                                    @csrf
                                                    <button class="btn btn-danger btn-sm">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
