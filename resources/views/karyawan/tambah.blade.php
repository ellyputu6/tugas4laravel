@extends('layout.main')

@section('title', 'Tambah Data')


@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong> TAMBAH DATA KARYAWAN </strong></h3>
                                <div class="pull-right">
                                    <a href="{{ url('data_kry') }}" class="btn btn-secondary btn-sm">
                                        <i class="fa fa-undo"></i> Back
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form action="{{ url('data_kry') }}" method="post">
                                            @csrf
                
                                            <div class="form-group">
                                                <label>Nama Karyawan</label>
                                                <input type="text" name="nama_karyawan" class="form-control @error('nama_karyawan') is-invalid @enderror" value="{{ old('nama_karyawan') }}" autofocus>
                                                @error('nama_karyawan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>No Karyawan</label>
                                                <input type="text" name="no_karyawan" class="form-control @error('no_karyawan') is-invalid @enderror" value="{{ old('no_karyawan') }}" autofocus>
                                                @error('no_karyawan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>No Telp</label>
                                                <input type="text" name="no_telp_karyawan" class="form-control @error('no_telp_karyawan') is-invalid @enderror" value="{{ old('no_telp_karyawan') }}" autofocus>
                                                @error('no_telp_karyawan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Jabatan</label>
                                                <input type="text" name="jabatan_karyawan" class="form-control @error('jabatan_karyawan') is-invalid @enderror" value="{{ old('jabatan_karyawan') }}" autofocus>
                                                @error('jabatan_karyawan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Divisi</label>
                                                <input type="text" name="divisi_karyawan" class="form-control @error('divisi_karyawan') is-invalid @enderror" value="{{ old('divisi_karyawan') }}" autofocus>
                                                @error('divisi_karyawan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
