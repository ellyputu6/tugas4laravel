<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function data()
    {
        $data = DB::table('data')->get();
        //return $data;
        return view('karyawan.datakaryawan', ['data' => $data]);
    }

    public function add ()
    {
        return view('karyawan.tambah');
    }

    public function addprocess(Request $request)
    {
        $request->validate([
            'nama_karyawan' => 'required|min:3|max:255',
            'no_karyawan' => 'required|min:2|max:15',
            'no_telp_karyawan' => 'required|min:10|max:255',
            'jabatan_karyawan' => 'required|min:10|max:255',
            'divisi_karyawan' => 'required|min:10|max:255',
        ]);

        DB::table('data')->insert([
            'nama_karyawan' => $request->nama_karyawan,
            'no_karyawan' => $request->no_karyawan,
            'no_telp_karyawan' => $request->no_telp_karyawan,
            'jabatan_karyawan' => $request->jabatan_karyawan,
            'divisi_karyawan' => $request->divisi_karyawan,
        ]);
        return redirect('data_kry')->with('status', 'Penambahan Data Karyawan Berhasil!');
    }

    public function delete($id)
    {
        DB::table('data')->where('id', $id)->delete();
        return redirect('data_kry')->with('status', 'Hapus Data Karyawan Berhasil!');
    }

    public function edit($id)
    {
        $editdata = DB::table('data')->where('id', $id)->first();
        return view('karyawan.update', compact('editdata'));
    }

    public function editprocess(Request $request, $id)
    {
        $request->validate([
            'nama_karyawan' => 'required|min:3|max:255',
            'no_karyawan' => 'required|min:2|max:15',
            'no_telp_karyawan' => 'required|min:10|max:255',
            'jabatan_karyawan' => 'required|min:10|max:255',
            'divisi_karyawan' => 'required|min:10|max:255',
        ]);
        
        DB::table('data')->where('id', $id)
            ->update([
                'nama_karyawan' => $request->nama_karyawan,
                'no_karyawan' => $request->no_karyawan,
                'no_telp_karyawan' => $request->no_telp_karyawan,
                'jabatan_karyawan' => $request->jabatan_karyawan,
                'divisi_karyawan' => $request->divisi_karyawan,
            ]);
        return redirect('data_kry')->with('status', 'Update Data Karyawan Berhasil!');
    }

}
