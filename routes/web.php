<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
Route::get('/home', [HomeController::class, 'home']);
Route::get('/datakaryawan', [DataController::class, 'data']);
Route::get('/data_kry/add', [DataController::class, 'add']);
Route::post('/data_kry', [DataController::class, 'addprocess']);
Route::delete('/data_kry/{id}', [DataController::class, 'delete']);
Route::get('/data_kry/edit/{id}', [DataController::class, 'edit']);
Route::patch('/data_kry/{id}', [DataController::class, 'editprocess']);